FROM node:latest

ENV NODE_ENV=production
COPY . /tsa
WORKDIR /tsa

RUN npm install --production

EXPOSE 443
CMD [ "node", "src/main.js", "timestamp", "443"]
