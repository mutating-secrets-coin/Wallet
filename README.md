# Wallet

A Proof of Concept of an idea to create more anonymous, censorship resistent digital currency without a blockchain and with a far lower entry threshhold.

For further details on the concept I have written two blog posts and discussed this on the Fediverse:
- [Mutating Secrets digital Currency](https://csdummi.codeberg.page/posts/cash/)
- [Mutating Secrets: A double spend resistent implementation with a trusted timestamping authority](https://csdummi.codeberg.page/posts/time/)


## Install
```
npm install .
```

#### Environment Variables
You need to have two environment variables `TSA_URI` and `TSA_PUBLICKEY` in order to run this program. I run a small test TSA at `blog.democraticnet.de` and have created a script to load these environment variables automatically.
You can either run:
```
./configure.sh
```

and have the environment variables
loaded automatically each session or you can run:

```
./start.sh
```

before each session manually.

## Commandline Interface
To use the commandline interface execute:
```
node src/main.js --help
```

### Receiving coins
All coins are stored as JSON files - which you are responsible for securing using encryption or other means.

You can either mint your own coins using the `mint` command or receive some coins from anothe person.

There are some coins already in this repository under [`coins/`](coins), that I try to keep up to date.

But once someboy added these coins to their local storage (using the `receive` command), you shouldn't be able to receive them again.

## Review & Security
This is a Proof-of-Concept test implementation. It is not secure, the core concept has not been properly reviewed and the implementation is probably insecure as well.

I'm open and encourage anyone to review my concept, my code and to play with this codebase.

## Contact
- [CSDUMMI on Mastodon](https://norden.social/@csddumi)

---

## Storage
For each coin a person owns this is kept in storage:
- The current secret and nonce.
- The history of secrets and nonces.
- The CIDs of the claims of the coin.

Each coin's data is encrypted using a passphrase and
stored in localStorage. The passphrase is kept in plain
text in sessionStorage and is forgotten after the sessions end.

## Glossary
- Coin: List of transactions started by a secret signed by the mint.
- Transaction: Tuple of secret and nonce.
- Transaction ID: Hash of the secret. Shared by all parallel transaction.
- Claim: Hash of a transaction, it's timestamp and the signature of the timestamp and hash.

## Claim & Verify
A coin can be entered into the wallet.

Following the entering of a new coin this algorithm is executed (secret, history, mintSig, claims):
```
claim()

timeout <- random
wait timeout

verify()

def claim():
  assert hash(coin.secret) not in claimsDB

  nonce <- random
  claim <- hash(coin.secret || nonce) || hash(coin.secret)

  timestamp <- request(TSA, claim) // TSA adds claim to claimsDB.

  new_secret <- hash(coin.secret || nonce || PAD)

  return new_secret

def verify():
  origin = history[0]

  // is coin issued by mint.
  if verify_sig(MINT_PUBLICKEY, mintSig, origin.secret):
    prev = origin

    for transaction in history[0:]:
      claims <- claimsDB[hash(transaction.secret)]

      wallet_claim <- claims[hash(transaction.secret || transaction.nonce )]

      // Is claim following prev
      if wallet_claim == hash(prev.secret, || prev.nonce) and transaction.secret == hash(prev.secret || prev.nonce || PAD):


        for claim in claims:
          // The claim is before the wallet's claim and it's timestamp is valid.
          // Then this coin is a result of double spending.
          if claim.timestamp < wallet_claim.timestamp and verify_sig(TSA_PUBLICKEY, claim.timestamp_signature, claim.timestamp):
            return False

    return True
  return False
```
