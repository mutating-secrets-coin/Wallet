module.exports = {
    TSA_URI: "https://blog.democraticnet.de",
    TSA_PUBLICKEY: "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuuBwl0dSNtJlcQ3UmS9D\nC8+qMpt/THh05Xt/jVL11S0VLOCBf/7KsSkwQaDRbnlWONzP81cGsxiWERAtukrm\nPiE8ZB7U19ke721GpS3NNbtcS8J2VhbXGxmG8OeidGoeQlDpIqhF0CjuXR4wQJn9\njjPdGZHaX9LjIau4p0H3g+J0lLEEArLag35GFR/6CJE0Ndax+xpFSUZO/L5absc7\neeYUewCwDoUVEdcAmqGcriZDH4K6FUbbPwTXCh9U7v1iHvX9Om0/Y8P8QVhfqAx5\ni1cKm1Z9VyGdujWVviPrekUdahoATvdTgeH6sMZ8iZ8V/pFb5ozBwfEcvkpER9rz\nJQIDAQAB\n-----END PUBLIC KEY-----\n",
    KEYFILE: "ssl/privkey.pem",
    CERTFILE: "ssl/cert.pem",
    SSLPASSPHRASE: undefined,
    DB: "/orbitdb/zdpuApHNXP9j3PHiTnmrrZx6KL52RaUHgU6mbhWc6NwEkVFCZ/blog.democraticnet.de",
}
