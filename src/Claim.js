const crypto = require("./crypto")
const uint8arrays = require("uint8arrays")

class Claim {
    constructor(id, hash, timestamp, signature) {
        this.id = id instanceof Uint8Array ? uint8arrays.toString(id, "base64url") : id
        this.hash = hash instanceof Uint8Array ? uint8arrays.toString(hash, "base64url") : hash
        this.timestamp = timestamp
        this.signature = signature
    }

    sign(privateKey) {
        this.signature = crypto.sign(this.serializeForSign(), privateKey)
    }

    verify(publicKey) {
        return crypto.verifySignature(this.signature, this.serializeForSign(), publicKey)
    }

    serializeForSign() {
        return JSON.stringify([
            this.id,
            this.hash,
            this.timestamp,
        ])
    }

    toJSON() {
        return {
            id: this.id,
            hash: this.hash,
            timestamp: toNull(this.timestamp),
            signature: toNull(this.signature),
        }
    }

    static fromJSON(obj) {
        return new Claim(obj.id, obj.hash, toNull(obj.timestamp), toNull(obj.signature))
    }
}

function toNull(x) {
    return x === undefined ? null : x
}

module.exports = Claim
