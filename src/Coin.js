const {
  getRandomBytes,
  randrange,
  hash,
  DIGEST_SIZE,
  sign,
  verifySignature,
} = require("./crypto")
const crypto = require("./crypto")
const Claim = require("./Claim")
const fetch = require("node-fetch")
const {
  Headers
} = require("node-fetch")
const uint8arrays = require("uint8arrays")

const {
    TSA_PUBLICKEY,
    TSA_URI
} = require("../config.js")

const PAD = new Uint8Array([1, 2, 223, 12, 244, 54, 155, 93])


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

// Merkle Tree
class TreeNode {
  /**
   * Node in a merkle tree.
   * @param data {Uint8Array} the data this node should hold.
   * @param branches {Array<TreeNode>} the branches of this node.
   * Only use data if this node is a leaf.
   * @param digest {Uint8Array} the digest of the branches and data to use as digest of the node.
   */
  constructor(data = new Uint8Array(), branches = [], digest = null) {
    this.data = data
    this.branches = branches
    this._digest = digest
  }

  addBranch(node) {
    this.branches.push(node)
    this._digest = null
  }

  get digest() {
    if (this._digest == null) {
      let branches = this.branches.map(branch => branch.digest)

      this._digest = hash(uint8arrays.concat([
        ...branches,
        this.data,
      ]))
    }
    return this._digest
  }

  toJSON() {
    return {
      branches: this.branches.map(b => b.toJSON()),
      data: uint8arrays.toString(this.data, "base64url"),
      digest: uint8arrays.toString(this.digest, "base64url"),
    }
  }

  static fromJSON(obj) {
    return new TreeNode(
      uint8arrays.fromString(obj.data, "base64url"),
      obj.branches.map(b => b.fromJSON()),
      uint8arrays.fromString(obj.digest, "base64url"),
    )
  }

  equals(node) {
    return this.digest == node.digest
  }
}

class Coin {
  /**
   * A coin that can be spend. But not double spend.
   * @param history {TreeNode} of secrets and nonces of a coin going back to the genesis secret.
   * @param mint {string} public key
   * @param mintSignature {Uint8Array} signature of the genesis secret
   */
  constructor(history, mint, mintSignature) {
    this.history = history
    this.mint = mint
    this.mintSignature = mintSignature
    this._accepted = false
  }

  static mint(privateKey) {
    let genesisSecret = getRandomBytes(DIGEST_SIZE)
    let mintSignature = crypto.sign(genesisSecret, privateKey)

    const publicKey = crypto.publicKey(privateKey)

    return new Coin(new TreeNode(genesisSecret), publicKey, mintSignature)
  }

  mutate() {
    let history = new TreeNode()

    history.addBranch(this.history)
    history.addBranch(new TreeNode(getRandomBytes(DIGEST_SIZE)))

    this.history = history
  }

  /**
   * Claim a coin, then mutate it.
   */
  async claim() {
    let id = hash(this.history.digest)

    this.mutate()

    let claim = new Claim(id, hash(uint8arrays.concat([
            this.history.branches[0].digest,
            this.history.branches[1].data,
            PAD,
        ])), null, null)

    let response = await fetch(`${TSA_URI}/timestamp`, {
      method: "POST",
      headers: new Headers({
        "X-Claim": JSON.stringify(claim)
      })
    })

    let {
      timestamp,
      signature,
    } = await response.json()

    claim.timestamp = timestamp
    claim.signature = signature

    return claim.verify(TSA_PUBLICKEY)
  }

  /**
   * @param claims orbitdb database containing the claims made for a currency.
   */
  verify(claims) {
    let node = this.history

    if(this.verifyHistory(node,claims)) {
        let genesisSecret = node.data
        return verifySignature(this.mintSignature, genesisSecret, this.mint)
    } else {
        return false
    }
  }

  /**
   * Verify the transaction history by verifyinging each transaction has a valid claim.
   */
  verifyHistory(node, db) {
    if(this.verifyClaim(node, db)) {
        if(node.branches != []) {
            return this.verifyHistory(node.branches[0], db)
        }
        return true
    }

    return false
  }

  /**
   * Verify one node in transaction history holds the first valid claim in the database.
   * @param node {TreeNode} of the transaction
   * @param db of claims.
   */
  verifyClaim(node, db) {
    let previousSecret = node.branches[0]
    let nonce = node.branches[1]

    let claims = db.query(claim => claim.id == uint8arrays.toString(hash(previousSecret.digest, "base64url")))

    claims = claims.map(Claim.fromJSON)

    // claim belonging to the nonce
    let own = claims.filter(claim => claim.hash == uint8arrays.toString(hash(uint8arrays.concat([previousSecret.digest, nonce.data, PAD])), "base64url"))

    if (own == []) {
      return false
    } else {
      own = own[0]
    }

    let earlierClaims = claims.filter(claim => claim.timestamp < own.timestamp)

    console.log("Earlier claims", earlierClaims)
    for (let claim of earlierClaims) {
        if (claim.verify(TSA_PUBLICKEY)) {
          console.log("Not verified")
          return false
        }
    }

    return true
  }

  /**
   * Accept a coin.
   * 1. claim coin
   * 2. wait
   * 3. verify coin
   * @param claims db
   * @return {boolean} indicating if the coin was/should be accepted
   */
  async accept(claims) {
    let claimed = await this.claim()
    console.log("Claimed", claimed)
    if (claimed) {
        await sleep(randrange(10, 120))

        this._accepted = this.verify(claims)
        console.log("Accepted", this._accepted)
        return this._accepted
    } else {
        return false
    }
  }

  get accepted() {
    return this._accepted
  }

  toJSON() {
    return {
      history: this.history.toJSON(),
      mint: this.mint,
      mintSignature: this.mintSignature,
    }
  }

  static fromJSON(obj) {
    return new Coin(TreeNode.fromJSON(obj.history), obj.mint, obj.mintSignature)
  }

  toString() {
    return `<Coin by ${uint8arrays.toString(hash(this.mint), "base64url")} ${this.accepted}>`
  }
}

module.exports = Coin
