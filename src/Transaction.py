from Crypto.Hash import SHA3_256
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from functools import reduce
from multiprocessing import Pool
import coin_pb2 as pb
import requests
import os
import sys
import base64
import time
import json

TSA_URI = os.environ.get("TSA_URI", default = "https://tsa.example.org")
TSA_PUBLICKEY = RSA.import_key(os.environ.get("TSA_PUBLICKEY", default = open("tsa.pem").read()))

PAD = b"19421345"

# UTILITIES
def randrange(a,b):
    return int.from_bytes(get_random_bytes(4), sys.byteorder)/2**32*(a - b) + b

# Merkle Tree
class TreeNode():
    """Implementation of a merkle tree
    for representing a coin.
    """
    def __init__(self, data = b"", branches = [], digest = None):
        """data: Data held by this node if any. Only use this for leafs in a merkle tree.
        branches: List of TreeNodes that are branches of this node.
        """
        self.data = data
        self.branches = []
        self.__digest__ = digest

    def add(self, node):
        self.branches.append(node)
        self.__digest__ = None

    def digest(self):
        if self.__digest__ is None:
            data = reduce(lambda a, b: a + b, [branch.digest() for branch in self.branches], b"") + self.data
            self.__digest__ = SHA3_256.new(data = data).digest()

        return self.__digest__

    def __dict__(self):
        return {
            "branches" : [branch.__dict__ for branch in self.branches],
            "data" : self.data,
            "digest": self.digest(),
        }

    def __encode__(self):
        node = pb.TreeNode()
        node.data = self.data
        node.branches.extend([branch.__encode__() for branch in self.branches])
        node.digest = self.digest()
        return node

    def encode(self):
        return self.__encode__().SerializeToString()

    @classmethod
    def decode(cls, packet):
        node = pb.TreeNode.ParseFromString(packet)
        branches = [TreeNode.decode(branch) for branch in node.branches]
        return cls(data = node.data, branches = node.branches, digest = node.digest)

class Coin:
    def __init__(self, history : TreeNode, mint : str, mintSignature : bytes):
        self.history = history
        self.mint = RSA.import_key(mint)
        self.mintSignature = mintSignature

    # MINT
    @classmethod
    def mint(cls, privateKey):
        genesis_secret = get_random_bytes(32)
        h = SHA3_256.new(genesis_secret)
        mintSignature = pkcs1_15.new(privateKey).sign(h)

        return cls(TreeNode(data = genesis_secret), privateKey.publickey().export_key("PEM").decode("utf-8"), mintSignature)

    # TRANSFER
    def mutate(self):
        """A mutation is applied after a transfer.
        """
        self.history = TreeNode(branches = [
                self.history, #recursion - it's fun
                TreeNode(data = get_random_bytes(32)),
            ])

    def claim(self):
        """A Claim is submitted before a mutation applied.
        """
        claim = pb.Claim()
        claim.id = SHA3_256.new(self.history.digest()).digest()

        self.mutate()

        # previous digest + nonce + pad
        claim.hash = SHA3_256.new(self.history.branches[0].digest() + self.history.branches[1].data + PAD).digest()

        response = requests.post(f"{TSA_URI}/timestamp", data = {
            "claim" : base64.b64encode(claim.SerializeToString(), b"-_")
            })

        claim = pb.Claim.ParseFromString(base64.b64decode(response.text, b"-_"))

        signature = claim.signature
        claim.signature = None

        h = SHA3_256.new(claim.SerializeToString())
        return pkcs1_15.new(TSA_PUBLICKEY).verify(h, signature)

    def verify(self):
        """Verify that all the transaction
        posses the earliest claim for their previous secret.

        And that the genesis secret is signed by the mint.
        """
        node = self.history

        while node.branches != []:
            if not self.verifyClaim(node):
                return False

            node = node.branches[0]

        genesis_secret = node.data

        h = SHA3_256.new(genesis_secret)
        return pkcs1_15.new(self.mint).verify(h, self.mintSignature)

    def verifyClaim(self, node):
        """Verification in principle:
        Check if a given transaction is
        being claimed by the earliest existing claim.

        Example:
         \ /
        -----   -----
        | C |   | B |
        -----   -----
           \     /
            \   /
            -----
            | A |
            -----
        A: secret to verify.
        B: Nonce
        C: previous secret

        1. fetch all claims with Hash(A + B)
        2. own <- claim where claim.hash == Hash(B + C + PAD)
        3. Is there a claim with earlier timestamp than own?
        yes-> 4. Is the earlier claim's signature valid?
            yes->return False
            no->return True
        no->return True
        """
        previous_secret = node.branches[0]
        nonce = node.branches[1]

        response = requests.get(CLAIMS_URI, data = {
            "id": base64.b64encode(SHA3_256.new(previous_secret.digest()).digest(), b"-_")
            })

        claims = [pb.Claim.ParseFromString(claim) for claim in response.json()]

        # Claim with the same hash
        own = [claim for claim in claims if claim.hash == SHA3_256(previous_secret.digest() + nonce.data + PAD).digest()]

        if own != []:
            own = own[0]
        else:
            return False

        earlier_claims = [claim for claim in claims if claim.timestamp < own.timestamp]

        # exists any earlier claim?
        if earlier_claims != []:
            # is the claim valid?
            for claim in earlier_claims:
                signature = claim.signature
                claim.signature = None
                h = SHA3_256.new(claim.SerializeToString())

                if pkcs1_15.new(TSA_PUBLICKEY).verify(h ,signature):
                    return False

        return True

    def accept(self):
        """Executed when accepting a coin into a wallet.
        1. claim coin
        2. timout
        3. verify coin
        """
        isClaimed = self.claim()
        time.sleep(randrange(1, 120))
        isVerified = self.verify()

        return isClaimed and isVerified

    # CODING
    def __dict__(self):
        return {
            "history": self.history.__dict__(),
            "mintSignature": self.signature,
        }

    def __encode__(self):
        coin = pb.Coin()
        coin.history.CopyFrom(self.history.__encode__())
        coin.mint = self.mint.export_key("PEM")
        coin.mintSignature = self.mintSignature

        return coin

    def encode(self):
        return self.__encode__().SerializeToString()

    @classmethod
    def decode(cls, packet):
        coin = pb.Coin.ParseFromString(packet)

        return cls(coin.history, coin.mint, coin.mintSignature)

def printCoins(coins):
    issuers = set([coin.mint for coin in coins])
    coins = {
        issuer: [coin for coin in coins if coin.mint == issuer]
        for issuer in issuers
    }

    for issuer in coins.keys():
        print(f"{issuer}:\t{len(coins[issuer])}")


if __name__ == "__main__":
    print("Wallet starting...", file = sys.stderr)
    print("? for help", file = sys.stderr)

    command = sys.argv[1:]

    if command[0] in ["?", "help"]:
        print("""Commands:
mint <amount>       Mint a new coin and output coinsfile
transfer <amount>   Takes amount coins out of ~/.coins and creates a coinsfile to transfer them.
receive <coinsfile> Receive a coin via a coinsfile and adds it to the local coins (~/.coins)
?                   show this help
#help               show this help
            """)

    elif command[0] == "mint":
        passphrase = os.environ.get("MINT_PASSPHRASE", default = None)
        privkey = RSA.import_key(open(os.environ["PRIVKEYFILE"]).read(), passphrase = passphrase)

        amount = int(command[1])
        coins = []

        for i in range(amount):
            coins.append(Coin.mint(privkey).encode())

        print(json.dumps([base64.b64encode(coin, b"-_").decode("utf-8") for coin in coins]))

    elif command[0] == "transfer":
        amount = int(command[1])

        coins = json.load(open(f"{os.environ['HOME']}/.coins"))

        if len(coins) < amount:
            print("Don't have enough coins to transfer this amount")


        transfer_coins = coins[:amount]

        print(json.dumps(transer_coins))

        json.dump(coins[:amount], open(f"{os.environ['HOME']}/.coins"))
    elif command[0] == "receive":
        coins = json.load(open(command[1]))
        coins_store = json.load()

        coins = [Coin.decode(coin) for coin in coins]
        with Pool(len(coins)) as p:
            accepted = p.map(lambda coin: coin.accept(), coins)

            coins = [coin for coin, accepted in zip(coins, accepted) if accepted]
            printCoins(coins)
