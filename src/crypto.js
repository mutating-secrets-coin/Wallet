const crypto = require("crypto")
const uint8arrays = require("uint8arrays")


function getRandomBytes(number) {
  return new Uint8Array(crypto.randomBytes(number))
}

function randrange(a, b) {
  return crypto.randomInt(a, b)
}

function hash(data) {
  const h = crypto.createHash("SHA3-256")
  h.update(data)

  return new Uint8Array(h.digest())
}

const DIGEST_SIZE = 32

function sign(data, privateKey) {
  const sign = crypto.createSign("SHA3-256")
  sign.write(data)
  sign.end()
  return uint8arrays.toString(new Uint8Array(sign.sign(privateKey)), "base64url")
}

function verifySignature(signature, data, publicKey) {
  console.log("Verify", signature)
  signature = uint8arrays.fromString(signature, "base64url")
  const verify = crypto.createVerify("SHA3-256")
  verify.update(data)
  verify.end()
  return verify.verify(publicKey, signature)
}

function publicKey(privateKey) {
        return crypto.createPublicKey(privateKey).export({ type: "spki", format: "pem" })
}

module.exports = {
  getRandomBytes,
  randrange,
  hash,
  DIGEST_SIZE,
  sign,
  verifySignature,
  publicKey,
}
