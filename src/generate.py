from Crypto.PublicKey import RSA

if __name__ == "__main__":
    print(RSA.generate(4096).export_key("PEM").decode("utf-8"))
