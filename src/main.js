#!/usr/bin/env node

/**
 * CLI Interface to store, mint, receive and send
 * coins.
 */


const Coin = require("./Coin")
const Claim = require("./Claim")
const IPFS = require("ipfs")
const IPFSHttpApi = require("ipfs-http-client")
const {
  multiaddr
} = require("multiaddr")
const OrbitDB = require("orbit-db")
const uint8arrays = require("uint8arrays")
const fetch = require("node-fetch")
const fs = require("fs")
const express = require("express")
const https = require("https")
const {
  getRandomBytes,
  randrange,
  hash,
  DIGEST_SIZE,
  sign,
  verifySignature,
} = require("./crypto")
const crypto = require("./crypto")

const {
    TSA_PUBLICKEY,
    TSA_URI,
    CERTFILE,
    KEYFILE,
    SSLPASSPHRASE,
} = require("../config.js")
const config = require("../config")


require('yargs')
  .scriptName("coins")
  .usage("$0 <cmd> [args]")
  .option("privatekey", {
    type: "string",
    default: "privkey.pem",
    describe: "Private Key File",
    alias: "priv",
  })
  .option("publickey", {
    type: "string",
    default: "pubkey.pem",
    describe: "Public Key File",
    alias: "pub",
  })
  .command("mint [amount]", "mint new coins", (yargs) => {
    yargs.positional("amount", {
      type: "number",
      default: 5,
      describe: "Number of new coins to mint",
    })
  }, function(argv) {
    const PRIVATE_KEY = fs.readFileSync(argv.privatekey, "utf-8")
    const PUBLIC_KEY = fs.readFileSync(argv.publickey, "utf-8")
    const coins = []

    for (let i = 0; i < argv.amount; i++) {
      coins.push(Coin.mint(PRIVATE_KEY, PUBLIC_KEY))
    }

    console.error(`Minted: ${coins.length}`)
    console.log(JSON.stringify(coins))
  })
  .command("transfer [amount]", "create new coinsfile with specified amount", yargs => {
    yargs.positional("amount", {
      type: "number",
      default: 5,
      describe: "Number of coins to transfer"
    })

    yargs.option("source", {
      alias: "src",
      type: "string",
      default: "coins.json",
      describe: "Coinsfile to take coins out of",
    })

    yargs.option("verify", {
      type: "boolean",
      default: false,
      describe: "verify the coins in the new coinsfile",
    })
  }, async function(argv) {
    const claims = await setupClaimsDB()
    let coins = JSON.parse(fs.readFileSync(argv.source, "utf-8"))

    if (coins.length < argv.amount) {
      console.error(`Not enough coins in coinsfile: Only ${coins.length} coins available`)
    } else {

        let transferCoins = []
        let remainingCoins = coins.slice()

        for (let i = 0; i < argv.amount; i++) {
            let coin = Coin.fromJSON(coins[i])

            if (!argv.verify || coin.verify(claims)) {
                transferCoins.push(coin.toJSON())
                remainingCoins[i] = undefined
            }

            if (transferCoins.length >= argv.amount) {
                break
            }
        }

        remainingCoins = remainingCoins.filter(c => c !== undefined)

        if (transferCoins.length < argv.amount) {
            console.error(`Not enough valid coins in coinsfile: ${transferCoins.length} coins are valid`)
        } else {
            console.error(`Transfering ${transferCoins.length} coins into new coinsfile.\nSuccess`)
            fs.writeFileSync(argv.source, JSON.stringify(remainingCoins))
            console.log(JSON.stringify(transferCoins))
        }
    }

    process.exit(0)
  })
  .command("receive [coinsfile]", "receive coins", (yargs) => {
      yargs.positional('coinsfile', {
        type: 'string',
        default: 'transfer.json',
        describe: 'Coinsfile to receive'
      })
      yargs.option("destination", {
        alias: "dest",
        type: "string",
        default: "coins.json",
        describe: "Coinsfile to store received coins in",
      })
    },
    async function(argv) {
      const claims = await setupClaimsDB()


      let coins = JSON.parse(fs.readFileSync(argv.coinsfile, {
        encoding: "utf-8"
      }))

      coins = coins.map(coin => Coin.fromJSON(coin))

      console.log("Received coins", coins.length)
      coins = await pFilter(coins, coin => coin.accept(claims))
      console.log("Verified coins", coins.length)

      let mints = coins.reduce((mints, coin) => {
        if (mints[coin.mint] === undefined) {
          mints[coin.mint] = 0
        }
        mints[coin.mint] += 1
        return mints
      }, {})

      mints = Object
        .keys(mints)
        .map(mint => `${uint8arrays.toString(hash(mint), "base64url")}\t${mints[mint]}`)
        .join("\n")

      console.error(`${coins.length} coins received.\nMint\t\t\t\t\t\tAmount\n${mints}`)

      // Append coins to destination coins file
      coins = coins.map(c => c.toJSON())
      coins = [...JSON.parse(fs.readFileSync(argv.dest)), ...coins]
      fs.writeFileSync(argv.dest, JSON.stringify(coins))
      process.exit(0)
    })
  .command("timestamp", "Run timestamp authority", (yargs) => {
    yargs.option("address", {
      alias: "a",
      type: "string",
      describe: "Address of the OrbitDB address used for storing the Claims",
      default: "",
    })
    yargs.option("port", {
      alias: "p",
      type: "number",
      default: 3006,
      describe: "Port to run on",
    })
  }, async function(argv) {
    const PRIVATE_KEY = fs.readFileSync(argv.privatekey, "utf-8")
    const PUBLIC_KEY = crypto.publicKey(PRIVATE_KEY)

    const ipfs = await createIPFS()

    const orbitdb = await OrbitDB.createInstance(ipfs)
    const address = OrbitDB.isValidAddress(config.DB) ? OrbitDB.parseAddress(config.DB) : config.DB


    const db = await orbitdb.open(address, {
      create: true,
      type: "docstore",
      indexBy: "id",
    })

    console.log(`Opened ${db.address.toString()}`)

    const app = express()

    app.post("/timestamp", async (req, res) => {
      let claim = Claim.fromJSON(JSON.parse(req.headers["x-claim"]))

      claim.timestamp = Date.now()

      claim.sign(PRIVATE_KEY)

      console.log(claim.toJSON())
      await db.put(claim.toJSON(), {
        pin: true
      })

      console.log(`Claims for ${claim.id}`, db.get(claim.id).length)

      res.json({
        timestamp: claim.timestamp,
        signature: claim.signature,
      })
    })

    app.get("/info", async (req, res) => {
      res.json({
        PUBLIC_KEY: PUBLIC_KEY,
        ADDRESS: db.address.toString(),
        PEER: await ipfs.id(),
      })
    })

    https.createServer({
      key: fs.readFileSync(KEYFILE),
      cert: fs.readFileSync(CERTFILE),
      passphrase: SSLPASSPHRASE,
    }, app).listen(argv.port)

    console.log(`Listening on ${argv.port}`)
  })
  .help()
  .argv

/**
 * Filter with a predicate that returns a promise.
 * Credit: https://advancedweb.hu/how-to-use-async-functions-with-array-filter-in-javascript/
 */
async function pFilter(arr, predicate) {
    const results = await Promise.all(arr.map(predicate))

    console.log(results)
    return arr.filter((_v, index) => results[index])
}

/**
 * Connect to TSA claims db on OrbitDB.
 */
async function setupClaimsDB() {

  let response = await fetch(`${TSA_URI}/info`, {
    method: "GET"
  })
  let info = await response.json()

  const ipfs = await createIPFS()

  for (let addr of info.PEER.addresses) {
    try {
      await ipfs.swarm.connect(multiaddr(addr))
    } catch (e) {
      console.log(`error while connecting to ${addr}`)
      continue
    }
  }
  const orbitdb = await OrbitDB.createInstance(ipfs)

  const db = await orbitdb.open(OrbitDB.parseAddress(info.ADDRESS))

  await db.load()

  process.on("exit", async code => {
    await db.drop()
  })

  return db
}

async function createIPFS() {
  const ipfs = process.env.IPFS_HTTP
                ? IPFSHttpApi.create()
                : await IPFS.create({
                    repo: "./jsipfs",
                    silent: true,
                    relay: {
                        enabled: true,
                        hop: {
                            enabled: true,
                            active: false
                        }
                    }
                  })

  process.on("exit", async code => {
    await ipfs.stop()
  })

  return ipfs
}

/**
 * Wait until an event happens.
 */
async function dbEventPromise(db, eventName) {
    return new Promise((resolve, reject) => {
        let fn = (...args) => {
            resolve(...args)

            db.events.removeListener(eventName,fn)
        }
        db.events.on(eventName, fn)
    })
}

module.exports = {
    createIPFS,
    setupClaimsDB,
    pFilter,
}
